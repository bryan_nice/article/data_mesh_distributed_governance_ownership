# Three Capabilities to help Healthcare Organizations Effectively Adopt Data Mesh

[Published Here](https://www.thoughtworks.com/en-us/insights/articles/3-capabilities-healthcare-organizations-adopt-data-mesh)

Over the last few years, the COVID-19 pandemic has created significant challenges for Healthcare Organizations (HCOs).[1] This event exposed and amplified many underlying constraints within our healthcare system, including a lack of widespread equitable access to care, inefficiencies in care coordination efforts, and clinical staffing shortages.[1] As the world continues to recover from the pandemic, HCOs are now being challenged to solve these socio-technical challenges while providing equitable access to quality care at a lower cost.

Many HCOs are exploring Data Mesh principles to enable them to manage rapid changes and provide trusted analytics to support value-based care outcomes in a secure, governed manner. Data Mesh provides an approach to develop socio-technical capabilities for HCOs to evolve their organization.[2]

To effectively adopt these principles, HCOs need to put as much emphasis on the "socio" part of "socio-technical'', as they do the “technical” aspect. Practically, this means considering governance of data as a cultural, delivery, and leadership issue, as opposed to an IT-centric issue.

In this article, we’ll look at three socio-technical capabilities to help HCOs effectively adopt Data Mesh, and ultimately respond effectively to the challenges they face today.

## Capability #1: Experimentation Culture

When HCOs adopt new innovations to manage their data products, they often execute a lift-and-shift strategy to implement this change as quickly as possible. Typically, this strategy yields little in return, because space to learn is sacrificed to meet tight timelines. Worse still, inappropriately applied governance processes are applied to manage knowledge gaps, which leads them to dealing with the same issues before adopting the new technology.[10]

Instead, for adopting Data Mesh principles HCOs should follow a similar approach as a start-up. The start-up approach is a strategy that prioritizes iterative experimentation across the innovation life-cycle.

There are three phases to the innovation life-cycle; show, shift, and scale. With this approach HCOs fund their delivery like an investor and focus their effort on creating new value in each phase. The process starts with small experiments aligned to a larger Data Mesh vision using minimal viable products (MVP) as targets. In the ‘show’ phase, the team can focus on the MVP’s problem-solution fit[4] between Data Mesh platform self-service and data product delivery.

In the ‘shift’ phase, HCOs prioritize iterations for platform-organization fit[4] and expand on lessons learned to support a limited number of data product teams. After the Data Mesh platform team has smoothed out the process to rapidly onboard teams, then HCOs move into the scale phase to progressively onboard more data product teams from across their organization.[4]

Throughout the Data Mesh adoption journey, HCO leaders will need to intrinsically motivate[11] their teams and give them the psychological safety to make mistakes and learn at every iteration. To curate an experimentation culture to effectively adopt Data Mesh, HCOs have to actively provide an environment that motivates people with purpose, enable them to have autonomy, and support them to develop Data Mesh mastery.[11]

This means transitioning decision-making to the teams doing the work and leadership focuses on providing clarity of vision with context. Mistakes learned from each experiment and iteration are not punished. The faster delivery teams can go through the build, measure, learn feedback loop[3], the quicker HCOs will be able to transition to Data Mesh and gain lasting value.

## Capability #2: Federated Governance

Using Data Mesh to deliver data products at scale is foundational for HCOs to achieve value-based care (VBC) outcomes. To do it effectively, HCOs must apply governance and controls in the right places.

Often, HCOs follow a centralized paradigm for Data Governance, which ends up removing accountability from the people doing the work. In the long run, this creates delivery bottlenecks with heavy bureaucratic processes, which can restrict agility and limit how quickly domains can respond to new challenges. Data Mesh principles provide a method to rethink governance to support faster iterations at higher quality.

In a Data Mesh, governance is approached with a federated model. Federated data governance uses a stewardship model with a Community of Practice (CoP) to balance domain autonomy with guardrails for creating data products.[8] The CoP provides guidance on all data-related activities such as data classification, metadata definition, interoperability, discoverability, transformation, metric definition, analysis, and issue resolution.[2,5,6] Federation is a hub and spoke model to governance, with the CoP sitting at its center.[8]

Within this model non-iT leaders, such as clinical or public health leaders act as nominated Data Stewards. These stewards are part of the Evaluate-Direct-Monitor (EDM) cycle(9),, and are responsible for the long-term vision and management of the data products published within their domain.[2,5,6]

They prioritize delivery iterations with their technical delegates, who are responsible for the development of domain-specific data products. The two work closely together to design, model, and evolve their data products.[2]

## Capability #3: Compliance as Code

Within an HCO’s Data Mesh, changes made to both data products and self-serve platforms must be made in a secure, governed way. Compliance as Code is a methodology that integrates automation into delivery processes to audit changes and ensure compliance adherence.[7] This methodology leverages version control systems to maintain separation of duties, tag semantically versioned changes, maintain change provenance, and continuously monitor to prevent compliance drift.[7]

Policies are declared upfront by Data Stewards to specify the data classification of their data products. Then, security controls associated to data classification are enforced within the Continuous Delivery pipeline.[7] Every change is tied back to version control, and ticketing systems to capture all events within the deployment audit path.[7] Approval gates and checks are automated, where the first step is driven by the prioritization practices conducted by the Data Steward and Technical Delegate as they add work to their backlog.[7]

This associates who requested the change, acceptance criterion, and any compliance checks conducted. Then the delivery team can work on the requested change using their versioning control system. Automation configurations will prompt and capture reviewers attesting their approval before the data product change and self-serve platform change is released into a Data Mesh environment.[7] Compliance as Code emphasizes having Everything as Code instead of paperwork ranging from systems configuration to data product instantiation.[7]

Conclusion

Data products are foundational for HCOs to increase equitable access to care and enable care program efficacy. Data Mesh provides an approach for HCOs to deliver trusted analytics at scale and gain the insights and capabilities they need to tackle their biggest operational challenges.

By embedding the socio-technical capabilities explored in this article, HCOs can effectively adopt Data Mesh principles. These social, cultural, and technical changes will enable HCOs to manage and operationalize data products across the care continuum.

If you’d like to discover what Data Mesh could do for you, talk to us [URL], or visit [URL] to find out more.

## References

1. Technological change in health care delivery [Internet]. UC Berkeley Labor Center. 2020 [cited 2022 Nov 18]. Available from: https://laborcenter.berkeley.edu/technological-change-in-health-care-delivery/
2. Dehghani Z. Data mesh: Delivering data-driven value at scale. Sebastopol, CA: O’Reilly Media; 2022.
3. Ries E. The Lean Startup. New York, NY: Crown Publishing Group; 2011.
4. Maurya A. Running lean: Iterate from plan A to a plan that works. 3rd ed. Sebastopol, CA: O’Reilly Media; 2022.
5. Madsen L. Disrupting data governance: A call to action. Bradley Beach, NJ: Technics Publications; 2019.
6. Plotkin D. Data stewardship: An actionable guide to effective data management and data governance. Oxford, England: Morgan Kaufmann; 2014.
7. Bird J. DevOpsSec. O’Reilly Media; 2016.
8. M. Lynne Markus & Quang "Neo" Bui (2012) Going Concerns: The Governance of Interorganizational Coordination Hubs, Journal of Management Information Systems, 28:4, 163-198, DOI: 10.2753/MIS0742-1222280407
9. Moens, A., & Roberts, N. (2017). Not just an IT issue: Why governance of data should be on the agenda of every board director. Governance Directions, 69(2), 104-106.
10. Ford N, Parsons R, Kua P. Building evolutionary architectures: support constant change. First edition. Beijing: O’Reilly; 2017. 174 p.
11. Pink DH. Drive: the surprising truth about what motivates us. New York, NY: Riverhead Books; 2009. 242 p.
