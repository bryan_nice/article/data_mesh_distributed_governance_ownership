# Egeria - Open Metadata and Governance

## Manifesto<sup>1</sup>

+ The maintenance of metadata must be automated to scale to the sheer volumes and variety of data involved in modern business. Similarly the use of metadata should be used to drive the governance of data and create a business friendly logical interface to the data landscape.
+ The availability of metadata management must become ubiquitous in cloud platforms and large data platforms, such as Apache Hadoop so that the processing engines on these platforms can rely on its availability and build capability around it.
+ Metadata access must become open and remotely accessible so that tools from different vendors can work with metadata located on different platforms. 
+ This implies unique identifiers for metadata elements, some level of standardization in the types and formats for metadata and standard interfaces for manipulating metadata.
+ Wherever possible, discovery and maintenance of metadata has to be an integral part of all tools that access, change and move information.

## Data Governance Roles<sup>2</sup>

+ Data Governance defines how an organization will make best of data whilst also keeping it safe and managed with reasonable level of cost and resources
+ Done well, data governance creates a sense of responsibility for data across every person in an organization plus an appreciation of its value to their work

![roles](egeria_scope_responsibility.png)

+ Roles typically are clustered together defining related types of interactions that need to occur. 
+ Most organizations, they represent just part of an individual’s responsibility.

![role definitions](egeria_roles.png)

### Organizational Leadership

+ `Manager` role is responsible for coordinating the work of other employees, and approvals of governance actions occurring within their team<sup>3</sup>.
+ `Executive` role is responsible for leading the direction of an organization and accountable to an organization's compliance to regulation<sup>4</sup>.

### External Parties

+ `Supplier` role is a business partner providing goods and/or services to the organization<sup>5</sup>.
+ `Customer` role is responsible for choosing an asset offered by the organization and setting up agreements and contracts necessary before an asset can be used<sup>6</sup>.
+ `Regulator` role is an authority for a regulation where they define the rules, issue guidance on being compliant, and investigate suspected breaches in regulation<sup>7</sup>.

### Governance Leadership Roles

Roles occur within governance programs are in place:
+ `Data Officer` role is responsible for setting the data strategy and leading data governance while supporting asset owners to invest into capabilities, researching, and experimenting with latest technology<sup>8</sup>.
+ `Security Officer` <sup>9</sup>.
+ `Incident Owner` role is responsible for coordinating the activity and resolution of a specific incident is detected by an organization<sup>10</sup>.
+ `Subject Area Owner` role is responsible for defining a given subject area<sup>11</sup>.

### Data Privacy

Roles lead in data privacy discussions:
+ `Privacy Officer` role is responsible for ensuring an organization properly cares about their services and operations does not adversely impact the privacy of individuals<sup>12</sup>.
  + Defining scope of personal data and the subset of attributes considered sensitive personal data
  + Performing privacy impact assessments on digital services using personal data and ensuring recommendations are implemented
  + Ensuring asset owners identified, catalogued, and classified their data sets containing either personal data or sensitive personal data.
  + Ensuring offering owners have the digital services and systems support them identifying personal data they are using and consent.
  + Ensuring organization can support requests from data subjects.
  + Preparing to manage a data breach incident.
+ `Data Subject` role is the person where the personal data is about<sup>13</sup>.

### Data Management

Roles involved in the day-to-day use of data assets:
+ `Asset Owner` role is responsible for investing decisions related to an asset and they own one or the more of the following<sup>14</sup>.
  + data set
  + system processes and stores data
  + digital service using data sets and systems to support a service offered by a business.
  + make choices on investment in security infrastructure and support
  + license and service level agreements on data set usage and consumption
+ `Asset Consumer` role is an individual or process who consumes an asset<sup>15</sup>.
+ `Data Steward` role is responsible for ensuring data is fit for purpose, involved with correcting errors in data, improving metadata descriptions, and responding to questions about the data<sup>16</sup>.
+ `Data Custodian` role is responsible for ensuring data from a third party is managed and used according to its license<sup>17</sup>.

### Digital Services

Roles for building and using digital services:
+ `Advocate` role is responsible for selling the value of an asset to potential asset consumers<sup>18</sup>.
+ `Architect` role is responsible for structural design of digital systems ensuring it fit for purpose, resilient, secure, and cost effective<sup>19</sup>.
+ `Project Lead` role is responsible for the delivery of a project on time and within budget<sup>20</sup>.
+ `Developer` role is responsible for designing, coding, and testing software<sup>21</sup>.
+ `Systems Administrator` role is responsible for managing IT infrastructure of an organization<sup>22</sup>.

## The ODPi Data Privacy Pack<sup>23</sup>

### Why is data privacy program important?

+ Data privacy is being written into law in many regions today and this legislation/regulation is both broadening the scope of data covered and increasing the penalties for non-compliance. 
+ Being able to manage your organization so people’s rights relating to their data is a basic capability for doing business in many places.
+ Actions taken to ensure data privacy lead to higher levels of customer service, better internal efficiency and a more respectful working environment for employees by creating transparency in the way the organization operates and eliminating unnecessary processing and storing of data

### What does having a data privacy program entail?

+ Ensures an organization processes data about an individual with respect to an individual's wishes, whilst ensuring minimal data is used and retained for processing and properly protecting to prevent unauthorized third party can no access it for their own purpose.
+ Entails
  + defining the scope of data about individuals needs special treatment called personal data
    + within this category elements are identified as sensitive which requires additional care
  + documenting where personal data is stored and used
  + validating all processing of personal data is with consent of the individual concerned within a given subject area
  + creating, implementing, and enforcing governance controls ensuring any changes to the processing of personal data are recorded and validated according to the privacy policies.
  + providing capability within a digital service where the data subject can exercise their rights with respects to their data

### Getting started

+ Digital services operated by an organization need to identify the principle uses of personal data and were it is stored
+ Privacy officer need to appoint owners of digital services for each business area and have them create inventory of their digital services since they are responsible for the correct operation of the services

### Digital service lifecycle

![digital lifecycle](egeria_digital_lifecycle.png)

+ Data Value Assessment - review of the types of data expected to be captured and why they are needed 
+ Data Processing Impact Assessment - review of the likely impact on an individual (data subject) if their data is processed by this digital service. 
+ Data Processing Description - details of the data usage within a digital service. 
+ Data Processing Certification - certifies that the data processing description for the digital service accurately reflects its processing. 
+ Security Certification - certifies that the infrastructure where the digital service will run is compliant with the appropriate security standards. 
+ Contract including Minimal Data Processing Descriptions - these are the terms and conditions that an individual signs up to when they enroll with the service. This determines the legitimate interest of the digital service, which effectively states that this is the minimal use of personal data that the digital service needs to operate. 
+ Data Use Report shows the audit report of the data use for the data subject (and/or data controller if this service is a data processor.)
+ Personal Data Management - provides the controls for a data subject to exercise their rights. 
+ Data Export - Data subjects have the right to request their data stored by your digital services. 
+ Suspicious Activity Report - describes some activity that needs investigating. 
+ Data Breach Report - describes a data breach - what happened and the steps to recover from it. 
+ Data Breach Impact Assessment - covers the impact of the data breach on the data subjects involved.

## Digital Services<sup>24</sup>

+ Well defined interface for requesting actions and data
+ Interface may include one or a combination of the following:
  + A user interface, such as a browser, application (eg Mobile App), or specialized device that allows a person to interact with the service. 
  + An API that is accessed by another digital service. 
  + An output feed of data that other digital services can subscribe to to receive a copy of this data 
  + An input feed of data or requests for the digital service to process
+ Privacy regulations such as the EU’s General Data Protection Regulation require an organization to guarantee that they are only processing personal data with the consent of the data subject.

## The lifecycle of a digital service<sup>25</sup>

+ An open metadata repository can be used to maintain information about the digital service. 
+ This can link knowledge of th business model, governance requirements and implementation, making it possible for collaboration correct action and an audit history to be maintained throughout the lifetime of the digital service.

![lifecycle](egeria_lifecycle.png)

+ Digital Service Planning covers the work to evaluate the business value of developing and running the digital service. This will include creating estimates of its development costs, and cost to run plus the likely uptake of the service and resulting buisness value. Much of this will be done by the business owner supported by an architect to sketch out the design of the digital service. 
+ Digital Service Development covers the implementation of the digital service plus the support mechanisms, business operations, launch and marketing planning for the digital service. 
+ Digital Service Deployment covers the work to push the digital service into production. 
+ Digital Service Onboarding covers the sales/recruiting process to onboard users to the digital service. 
+ Digital Service Use covers the production use of the digital service. 
+ Digital Service Incident covers the management of an incident such as an outage, upgrade of essential infrastructure or data breach.

## References

1. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. The Challenge \[cited 2021-11-20\]; Available from:
    https://odpi.github.io/egeria-docs/introduction/challenge/](https://odpi.github.io/egeria-docs/introduction/challenge/)
2. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Data Governance Roles \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/](https://opengovernance.odpi.org/roles/)
3. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Manager Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/manager-role.html](https://opengovernance.odpi.org/roles/manager-role.html)
4. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Executive Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/executive-role.html](https://opengovernance.odpi.org/roles/executive-role.html)
5. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Supplier Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/supplier-role.html](https://opengovernance.odpi.org/roles/supplier-role.html)
6. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Customer Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/customer-role.html](https://opengovernance.odpi.org/roles/customer-role.html)
7. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Regulator Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/regulator-role.html](https://opengovernance.odpi.org/roles/regulator-role.html)
8. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Data Officer Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/data-officer-role.html](https://opengovernance.odpi.org/roles/data-officer-role.html)
9. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Security Officer Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/security-officer-role.html](https://opengovernance.odpi.org/roles/security-officer-role.html)
10. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Incident Owner Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/incident-owner-role.html](https://opengovernance.odpi.org/roles/incident-owner-role.html)
11. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Subject Area Owner Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/subject-area-owner-role.html](https://opengovernance.odpi.org/roles/subject-area-owner-role.html)
12. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Privacy Officer Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/privacy-officer-role.html](https://opengovernance.odpi.org/roles/privacy-officer-role.html)
13. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Data Subject Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/data-subject-role.html](https://opengovernance.odpi.org/roles/data-subject-role.html)
14. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Asset Owner Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/asset-owner-role.html](https://opengovernance.odpi.org/roles/asset-owner-role.html)
15. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Asset Consumer Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/asset-consumer-role.html](https://opengovernance.odpi.org/roles/asset-consumer-role.html)
16. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Data Steward Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/data-steward-role.html](https://opengovernance.odpi.org/roles/data-steward-role.html)
17. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Data Custodian Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/data-custodian-role.html](https://opengovernance.odpi.org/roles/data-custodian-role.html)
18. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Advocate Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/advocate-role.html](https://opengovernance.odpi.org/roles/advocate-role.html)
19. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Architect Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/architect-role.html](https://opengovernance.odpi.org/roles/architect-role.html)
20. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Project Lead Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/project-lead-role.html](https://opengovernance.odpi.org/roles/project-lead-role.html)
21. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Developer Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/developer-role.html](https://opengovernance.odpi.org/roles/developer-role.html)
22. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Systems Administrator Role \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/roles/systems-administrator-role.html](https://opengovernance.odpi.org/roles/systems-administrator-role.html)
23. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. The ODPi Data Privacy Pack \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/data-privacy-pack/](https://opengovernance.odpi.org/data-privacy-pack/)
24. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Digital Services \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/digital-services/](https://opengovernance.odpi.org/digital-services/)
25. [Egeria - Open Metadata and Governance \[Internet\]. \[place unknown\]: Egeria - Open Metadata and Governance \[date unknown\]. Digital Service Lifecycle \[cited 2021-11-20\]; Available from: https://opengovernance.odpi.org/digital-services/digital-service-lifecycle.html](https://opengovernance.odpi.org/digital-services/digital-service-lifecycle.html)
