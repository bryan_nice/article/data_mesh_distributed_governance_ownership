# Fybrik

## Introduction<sup>1</sup>

Roles:
+ Data users 
  + to use data in a self-service model without manual processes and without dealing with credentials.
  + Use common tools and frameworks for reading from and exporting data to data lakes or data warehouses.
  + input definitions related to data usage in the business logic of their applications
+ Data stewards 
  + to control data access and data usage by applications. 
  + Use the organization's policy manager and data catalog of choice and system enforce data usage policies even after data is accessed.
  + input definitions related to data governance and security
+ Data operators 
  + to automate data lifecycle management via implicit data copies, eliminating the need for manual versioning and copying of data.
  + input definitions related to infrastructure and available resources

![How does it work](fybrik_how_does_it_work.png)

System:
+ Integrates business logic with non-functional data centric requirements such as enabling data access regardless of its physical location, caching, lineage tracking, etc.
+ Enforce governance on the usage of data; including limiting what data the business logic can access, performing transformations as needed, controlling what the business logic can export and where to
+ Makes data available in locations where it is needed. Thus in a multi cluster scenario it may copy data from one location to another

### Applications<sup> </sup>

+ considers applications as first level entities. 
+ Before running a workload, an application needs to be registred to a system control plane by applying a system resource. 
+ This is the declarative definition provided by the data user. 
+ The registration provides context about the application such as the purpose for which it's running, the data assets that it needs, and a selector to identify the workload. 
+ Additional context such as geo-location is extracted from the platform.

### Security<sup>1</sup>

+ modules run in the data path to handle access to data, including passing the data access credentials to upstream data stores. 
+ Security is preserved by authorizing the applications based on their Pod identities.

## Architecture<sup>2</sup>

![architecture](fybrik_architecture.png)

1. Install modules and modules can provide read/write access or produce implicit copies that serve as lower latency caches of remote assets. Modules also enforce policies defined for data assets.
2. The data steward configures policies in an external policy manager over assets defined in an external data catalog. Dataset credentials are retrieved from Vault by using Vault API.
3. submits an application and controller will make sure that all the specs are fulfilled and will make sure that the data is made accessible to the user according to the previously defined policies. The system holds metadata about the application such the data assets required by the application, the processing purpose and the method of access the user wishes
4. It uses this information to check with external systems; if access or copy is allowed and whether restrictive policies such as masking or hashing have to be applied.
5. It compiles blueprints based on on the policy decisions received via the connectors and chooses the modules which are best fit for the requirements that the user specified regarding the access protocol and availability. 
6. As data assets may reside in different systems the blueprints are compiled in a Plotter CRD that specifies which blueprints have to be executed in which cluster.
7. distribute the blueprints by collecting statuses and distributes updates of blueprints. Once all the blueprints on all clusters are ready the plotter is marked as ready.
8. The BlueprintController makes sure that a blueprint can deploy all needed modules 
9. The BlueprintController makes sure that a blueprint can deploy all needed modules 
10. Tracks their status; an implicit-copy module finishes the copy the blueprint is also in a ready state.
11. Credentials are handled by the modules and are never exposed to the user and the application reads from and writes data to allowed targets. 
12. Requests are handled by instances and the application can not interact with unauthorized targets.

## Plugins<sup>3</sup>

+ two extension mechanisms, namely connectors and modules

### Connectors<sup>3</sup>

+ Connectors are GRPC services on the control plane uses to connect to external systems.
+ control plane needs connectors to data catalog and policy manager.
+ These connector GRPC services are deployed alongside the control plane.
+ Connector Types
  + Data catalog
    + provides metadata about the asset such as security tags. 
    + It also provides connection information to describe how to connect to the data source to consume the data. 
    + metadata provided by the catalog both to enable seamless connectivity to the data and as input to making policy decisions. 
    + The data user is not concerned with any of it and just selects the data that it needs regardless of where the data resides.
  + Policy manager
    + Enforcing data governance policies requires a Policy Decision Point (PDP) that dictates what enforcement actions need to take place
    + supports a wide and extendible set of enforcement actions to perform on data read, write or copy.
    + include transformation of data, verification of the data, and various restrictions on the external activity of an application that can acceess the data.
    + A PDP returns a list of enforcement actions given a set of policies and specific context about the application and the data it uses.

### Modules<sup>4</sup>

+ Modules are the way to describe such data plane components and make them available to the control plane. 
+ The control plane generates a description of a data plane based on policies and application requirements. 
+ This is known as a blueprint, and includes components that are deployed by the control plane to fulfill different data-centric requirements. 
+ The functionality described by the module may be deployed (a) per workload, or (b) it may be composed of one or more components that run independent of the workload and its associated control plane.
+ In the case of (a), the control plane handles the deployment of the functional component. 
+ In the case of (b) where the functionality of the module runs independently and handles requests from multiple workloads, a client module is what is deployed by the control plane. 
+ This client module passes parameters to the external component(s) and monitors the status and results of the requests to the external component(s).

![modules](fybrik_modules.png)

+ Registering a module
  + To make the control plane aware of the module so that it can be included in appropriate workload data flows, the administrator must apply the YAML in the namespace to make the control plane aware of the existence of the module.

## Vault<sup>5</sup>

+ standalone applications that Vault server executes to enable third-party secret engines and auth methods.

## Enable Control Plane Security<sup>6</sup>

+ traffic to connectors that run as part of the control plane must be secured. Follow this page to enable control plane security.

## Using OPA<sup>7</sup>

+ One simple approach is to use OPA kube-mgmt and manage Rego policies in Kubernetes Configmap resources.

## Multicluster Setup<sup>8</sup>

+ dynamic in its multi cluster capabilities in that it has abstractions to support multiple different cross-cluster orchestration mechanisms. 
+ Currently only one multi cluster orchestration mechanism is implemented and is using Razee for the orchestration.

## Reference

1. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Concepts Introduction \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/concepts/introduction/](https://fybrik.io/v0.4/concepts/introduction/)
2. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Concepts Architecture \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/concepts/architecture/](https://fybrik.io/v0.4/concepts/architecture/)
3. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Concepts Plugins: Connectors \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/concepts/connectors/](https://fybrik.io/v0.4/concepts/connectors/)
4. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Concepts Plugins: Modules \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/concepts/modules/](https://fybrik.io/v0.4/concepts/modules/)
5. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Concepts Plugins: HashiCorp Vault Plugins \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/concepts/vault_plugins/](https://fybrik.io/v0.4/concepts/vault_plugins/)
6. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Enable Control Plane Security \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/tasks/control-plane-security/](https://fybrik.io/v0.4/tasks/control-plane-security/)
7. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Using OPA \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/tasks/using-opa/](https://fybrik.io/v0.4/tasks/using-opa/)
8. [Fybrik \[Internet\]. \[place unknown\]: Fybrik \[date unknown\]. Multicluster Setup \[cited 2021-10-18\]. Available from: https://fybrik.io/v0.4/tasks/multicluster/](https://fybrik.io/v0.4/tasks/multicluster/)
