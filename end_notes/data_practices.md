# Data Practices

## Manifesto<sup>1</sup>

### Values

We believe these values and principles, taken together, describe the most effective, ethical, and modern approach to data teamwork.

+ Inclusion: Maximize diversity, connectivity, and accessibility among data projects, collaborators, and outputs.
+ Experimentation: Emphasize continuously iterative testing and data analysis.
+ Accountability: Behave ethically and transparently, fix mistakes quickly, and hold ourselves and others accountable.
+ Impact: Prioritize projects with well-defined goals, and design them to achieve measurable, substantive outcomes.

### Principles

As data teams, we aim to...

1. Use data to improve life for our users, customers, organizations, and communities. 
2. Create reproducible and extensible work. 
3. Build teams with diverse ideas, backgrounds, and strengths. 
4. Prioritize the continuous collection and availability of discussions and metadata.
5. Clearly identify the questions and objectives that drive each project and use to guide both planning and refinement. 
6. Be open to changing our methods and conclusions in response to new knowledge. 
7. Recognize and mitigate bias in ourselves and in the data we use. 
8. Present our work in ways that empower others to make better-informed decisions. 
9. Consider carefully the ethical implications of choices we make when using data, and the impacts of our work on individuals and society. 
10. Respect and invite fair criticism while promoting the identification and open discussion of errors, risks, and unintended consequences of our work. 
11. Protect the privacy and security of individuals represented in our data. 
12. Help others to understand the most useful and appropriate applications of data to solve real-world problems.

## References:

1. [Data Practices \[Internet\]. \[place unknown\]: Data Practices \[date unknown\]. Data Values & Principles \[cited 2021 Nov 20\]; Available from: https://datapractices.org/manifesto/](https://datapractices.org/manifesto/)
