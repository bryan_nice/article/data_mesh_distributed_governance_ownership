# How to Move Beyond a Monolithic Data Lake to a Distributed Data Mesh

## The Current Enterprise Data Platform Architecture

>***___Note___:***
> Zhamak Dehghani's architectural history

+ First generation: monolithic enterprise data warehouse with large price tags and large amounts of technical debt from unmaintainable ETL jobs, tables, and reports where small amount of specialized people understand.
+ Second generation: complex big data ecosystems with long running batch jobs maintained by a central team of hyper-specialized data engineers.
+ Third generation: unified batch and stream architectures for data transformations with open source frameworks and cloud services.

### Architectural Failure Modes

>***___Note___:***
> Zhamak Dehghani's claimed problem statement 

+ Tightly coupled pipeline, architecture, centralized, and monolithic
  + Ingest data from operational and transactional systems and domains across the entire organization.
  + Cleanse, enrich, and transform source data with big up front design attempting to unify taxonomies and ontologies into aggregate views
  + Serve data sets to a variety of consumers with a diverse set usage requirements spanning across analytical, exploratory, predictive, or reporting.
+ Domain driven design and bounded context architectural patterns are not applied to analytical data systems
+ Analytics data systems tend to have a hard time scaling
  + Ubiquitous data with source proliferation
  + Innovation agenda with consumer proliferation

## The Next Enterprise Data Platform Architecture (Paradigm Shift)

+ Distributed Domain Driven Data Architecture
  + Domain oriented data decomposition and ownership
  + source oriented domain data
  + consumer oriented and shared data domain
  + distributed pipelines as domain internal implementation
+ Self-Serve Platform Design
  + Scalable polyglot big data storage 
  + Encryption for data at rest and in motion 
  + Data product versioning 
  + Data product schema 
  + Data product de-identification 
  + Unified data access control and logging 
  + Data pipeline implementation and orchestration 
  + Data product discovery, catalog registration and publishing 
  + Data governance and standardization 
  + Data product lineage 
  + Data product monitoring/alerting/log 
  + Data product quality metrics (collection and sharing)
  + In memory data caching 
  + Federated identity management 
  + Compute and data locality
+ Data Product Thinking
  + domain data as product
  + discoverable
  + addressable
  + trustworthy and truthful
  + self-describing semantics and syntax
  + inter-operable and governed
  + secure and governed
  + cross functional teams

## Data Mesh Architectural Pattern

+ serving over ingesting
+ discovering and using over extracting and loading
+ Publishing events as streams over flowing data around via centralized pipelines
+ Ecosystem of data products over centralized data platform

## Reference

+ [Zhamak Dehghani. How to Move Beyond a Monolithic Data Lake to a Distributed Data Mesh\[Internet\]. Chicago, IL , US: MartinFowler.com; 2019-05-20. Available from: https://martinfowler.com/articles/data-monolith-to-mesh.html](https://martinfowler.com/articles/data-monolith-to-mesh.html)