# Data Mesh Principles and Logical Architecture

## The Great Divide of Data

>***___Note___:***
> Zhamak Dehghani's claimed problem statement

+ Architectural split between operational data and analytical data causes fragility
  + Data divergence between the two data context
  + Continuously failing ETL (Extract, Transform, Load) jobs
  + Ever increasing complexity between the two data context
+ Data lake and data warehouse are the two divergent analytical architectures and stacks
  + data lake for data science workloads and attempts to support data analyst workloads
  + data warehouse for data analyst workloads and attempts to support data science workloads
  + differing data consumer personas with diverse access patterns
  + Management and access to analytical data impedes scaling
+ Difference in technology leads to fragmentation in organizations
+ Data mesh is a microservice architecture and API access endpoints to solve this problem

## Principles

+ Domain-oriented decentralized data ownership and architecture
  + So that the ecosystem creating and consuming data can scale out as the number of sources of data, number of use cases, and diversity of access models to the data increases; simply increase the autonomous nodes on the mesh.
  + decompose and decentralize data ecosystem and ownership into components
+ Data as a product
  + Easily discoverable, understandable and securely access high quality data
  + Distributed across many domains
+ Self-serve data infrastructure as a platform
  + Domain teams can create and consume data products autonomously 
  + Using platform abstractions, hiding the complexity of building, executing and maintaining secure and interoperable data products
+ Federated computational governance
  + Get value from aggregation and correlation of independent data products
  + Behaves as an ecosystem following global interoperability standards
  + Standards that are baked computationally into the platform.

## Reference

+ [Zhamak Dehghani. Data Mesh Principles and Logical Architecture \[Internet\]. Chicago, IL , US: MartinFowler.com; 2020-10-09. Available from: https://martinfowler.com/articles/data-mesh-principles.html](https://martinfowler.com/articles/data-mesh-principles.html)
